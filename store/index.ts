// import { configureStore } from "@reduxjs/toolkit";
// import budgetReducer from "./budgetSlice";

// export default configureStore({
//   reducer: {
//     budget: budgetReducer,
//   },
// });
import { combineReducers } from 'redux';
import { configureStore } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import budgetReducer from './budgetSlice';

// Combine your reducers
const rootReducer = combineReducers({
  budget: budgetReducer,
  // Add other reducers if any
});

// Configure Redux persist options
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

// Create a persisted reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);

// Create the Redux store
const store = configureStore({
  reducer: persistedReducer,
});

// Create the persistor
export const persistor = persistStore(store);

export default store;
