# My React Native App

## Installation

Before you start, make sure you have Node.js and npm installed on your machine. You can download and install them from [here](https://nodejs.org/).

## Usage

To run the app, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the project directory in your terminal.
3. Install dependencies by running:
   ```bash
   npm install
   ```

-> Once the dependencies are installed, start the Expo development server by running:

```bash
npx expo start
```

->This will start the development server and open Expo DevTools in your default web browser.

->That's it! You can now run your app on an emulator, physical device, or web browser using the Expo Go app.
