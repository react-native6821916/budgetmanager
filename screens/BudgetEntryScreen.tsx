import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, Button, TouchableOpacity, ToastAndroid, Platform } from 'react-native';
import { useDispatch } from 'react-redux';
import { addEntry } from '../store/budgetSlice';
import Toast from 'react-native-toast-message';
import { Alert } from 'react-native';



const BudgetEntryScreen = ({navigation}) => {
  const [name, setName] = useState('');
  const [plannedAmount, setPlannedAmount] = useState('');
  const [actualAmount, setActualAmount] = useState('');

  const dispatch = useDispatch();


  const saveEntry = () => {

    if (!name.trim() || !plannedAmount || !actualAmount) {
        if (Platform.OS === 'android') {
            ToastAndroid.show("Please fill all fields", ToastAndroid.SHORT)
          } else {
            Alert.alert("Please fill all fields");
          }
        return; 
      }

    dispatch(
      addEntry({
        name,
        plannedAmount: parseFloat(plannedAmount),
        actualAmount: parseFloat(actualAmount),
      })
    );
    setName('');
    setPlannedAmount('');
    setActualAmount('');

     
    if (Platform.OS === 'android') {
        ToastAndroid.show("Entry saved successfully!", ToastAndroid.SHORT)
      } else {
        Alert.alert("Entry saved successfully!");
      }
  };

  const navigateToBudgetListing = () => {
    navigation.navigate('BudgetEntryListing'); 
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Name"
        value={name}
        onChangeText={setName}
      />
      <TextInput
        style={styles.input}
        placeholder="Planned Amount"
        value={plannedAmount}
        onChangeText={setPlannedAmount}
        keyboardType="numeric"
      />
      <TextInput
        style={styles.input}
        placeholder="Actual Amount"
        value={actualAmount}
        onChangeText={setActualAmount}
        keyboardType="numeric"
      />
      <TouchableOpacity style={styles.button} onPress={saveEntry}>
        <Text style={styles.buttonText}>Save</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={navigateToBudgetListing}>
        <Text style={styles.buttonText}>Show Items</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  input: {
    width: '100%',
    marginBottom: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
  },
  button: {
    width: '100%',
    backgroundColor: '#007bff',
    padding: 15,
    borderRadius: 5,
    marginTop: 10,
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default BudgetEntryScreen;
