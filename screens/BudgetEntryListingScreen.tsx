import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { useSelector } from 'react-redux';
import { Card, Divider } from 'react-native-paper';

const BudgetEntryListingScreen = () => {
  const entries = useSelector((state: any) => state.budget.entries);

  if(entries.length === 0){
    return (
      <View style={styles.container}>
        <Text style={styles.noEntry}>Please Add a Budget Entry First!</Text>
      </View>
    );
  }

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      {entries.map((entry, index) => (
        <View key={index} style={styles.entryContainer}>
          <Text style={styles.entryText}>Name: {entry.name}</Text>
          <Text style={styles.entryText}>Planned Amount: {entry.plannedAmount}</Text>
          <Text style={styles.entryText}>Actual Amount: {entry.actualAmount}</Text>
        </View>
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  scrollContainer: {
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  noEntry: {
    fontSize: 20,
    textAlign: 'center',
  },
  entryContainer: {
    marginBottom: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    backgroundColor: '#f9f9f9',
  },
  entryText: {
    fontSize: 16,
    marginBottom: 5,
  },
});



export default BudgetEntryListingScreen;
