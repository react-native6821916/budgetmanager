import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Provider } from "react-redux";
import BudgetEntryListingScreen from "./screens/BudgetEntryListingScreen";
import BudgetEntryScreen from "./screens/BudgetEntryScreen";
import store from "./store";

const Stack = createStackNavigator();

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="BudgetEntry" component={BudgetEntryScreen} />
          <Stack.Screen
            name="BudgetEntryListing"
            component={BudgetEntryListingScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
